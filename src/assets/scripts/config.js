/**
 * RequireJS configuration file.
 *
 * The RequireJS configurations should be declared in this file. Paths to
 * libraries (custom and vendors) should be specified including their shims.
 */

require.config({
    paths: {
        'requirejs': './../vendors/requirejs/require',
        'text': './../vendors/requirejs-text/text',
        'shader': './../vendors/shader-text/shader',
        'bind-polyfill': './../vendors/bind-polyfill/bind.polyfill',
        'jquery': './../vendors/jquery/dist/jquery.min',
        'threejs': './../vendors/threejs/build/three.min',
        //'threejs': './../vendors/threejs/build/three',
        'underscore': './../vendors/underscore/underscore-min',
        'logger': './framework/core/logger'
    },

    shim: {
        'threejs': {
            exports: 'THREE'
        },

        'underscore': {
            exports: '_'
        }
    },

    waitSeconds: 60
});

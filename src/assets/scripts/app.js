define(function(require, exports, module) { // jshint ignore:line
    'use strict';

    require('bind-polyfill');
    require('text');
    require('shader');
    require('threejs');
    require('underscore');

    var $ = require('jquery');
    var GraphicsDriver = require('./framework/graphics/graphicsDriver');

    var GameScene = require('./scenes/gameScene');

    var Settings = require('./framework/core/settings');
    var AssetLoader = require('./framework/core/assetLoader');
    var Camera = require('./framework/core/camera');
    var KeyInput = require('./framework/input/keyInput');

    var UIManager = require('./framework/ui/uiManager');

    /**
     * The App class acts as the core bootstrap class for
     * starting up the different modules available to the
     * application.
     *
     * @class App
     * @constructor
     */
    var App = function() {
        /**
         * The graphics driver for rendering to DOM.
         *
         * @property graphicsDriver
         * @type {GraphicsDriver}
         */
        this.graphicsDriver = null;

        // Initialize our application.
        this.init();
    };

    var proto = App.prototype;

    /**
     * Initialize the application.
     *
     * @method init
     * @returns {App}
     * @private
     */
    proto.init = function() {
        this.setupChildren();

        return this;
    };

    /**
     * Creates/assigns the children objects
     * of the application.
     *
     * @method setupChildren
     * @returns {App}
     * @private
     */
    proto.setupChildren = function() {
        this.$gameAppElement = $('.gameView');

        this.settings = new Settings();

        this.assetLoader = new AssetLoader(this);

        this.uiManager = new UIManager(this);

        this.input = {};
        this.input.keyInput = new KeyInput();

        this.graphicsDriver = new GraphicsDriver(this.$gameAppElement, this);
        this.graphicsDriver.init(GraphicsDriver.GD_WEBGL_TYPE);

        this.camera = new Camera(this.graphicsDriver.getViewport());
        this.camera.init();

        this.graphicsDriver.setCurrentCamera(this.camera);

        this.scene = new GameScene();
        this.scene.init(this, this.camera);

        this.graphicsDriver.setCurrentScene(this.scene);

        this.graphicsDriver.render();

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the game application element.
     *
     * @method getGameElement
     * @returns {jQuery<HTMLObject>}
     */
    proto.getGameElement = function() {
        return this.$gameAppElement;
    };

    /**
     * Returns the graphics driver.
     *
     * @method getGraphicsDriver
     * @returns {GraphicsDriver}
     */
    proto.getGraphicsDriver = function() {
        return this.graphicsDriver;
    };

    /**
     * Returns the game settings object.
     *
     * @method getSettings
     * @returns {Settings}
     */
    proto.getSettings = function() {
        return this.settings;
    };

    /**
     * Returns the UIManager object.
     *
     * @method getUIManager
     * @returns {UIManager}
     */
    proto.getUIManager = function() {
        return this.uiManager;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Helpers
    ///////////////////////////////////////////////////////////////////////////

    return App;
});

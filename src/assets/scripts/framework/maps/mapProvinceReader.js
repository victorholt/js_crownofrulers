define(function(require) {
    'use strict';

    /**
     * Reads a province-based image map and builds
     * an array of provinces.
     *
     * @class MapProvinceReader
     * @constructor
     */
    var MapProvinceReader = function(graphicsDriver) {
        /**
         * Reference to the GraphicsDriver object.
         *
         * @property graphicsDriver
         * @type {GraphicsDriver}
         */
        this.graphicsDriver = graphicsDriver;
    };

    var proto = MapProvinceReader.prototype;

    /**
     * Reads an image and returns an array of
     * province models.
     *
     * @method readImage
     * @returns {Array<Province>}
     */
    proto.readImage = function(imageFilePath) {
        var image = new Image();
        image.onload = this.onImageLoaded.bind(this, image);
        image.src = imageFilePath;
    };

    /**
     * This event is called after the image has been loaded.
     *
     * @method onImageLoaded
     * @returns {void}
     */
    proto.onImageLoaded = function(image) {
        if (image == null) {
            image = this;
        }

        var colors = {};
        var canvas = document.createElement('canvas');
        var canvasContext = canvas.getContext('2d');
        canvas.width = image.width;
        canvas.height = image.height;
        canvasContext.drawImage(image, 0, 0);

        var imageData = canvasContext.getImageData(0, 0, image.height, image.width);

        var x = 0;
        var y = 0;
        var count = 0;
        var length = imageData.data.length;
        var r = 0;
        var g = 0;
        var b = 0;
        var a = 0;
        var colorCount = 0;

        console.log('image: ' + image.width + 'x' + image.height);
        console.log('image data length: ' + length);

        while (count < length) {
            r = imageData.data[count + 0];
            g = imageData.data[count + 1];
            b = imageData.data[count + 2];
            a = imageData.data[count + 3];

            var color = 'color_' + r + '_' + g + '_' + b +'_' + a;
            if (colors[color] == null) {
                colors[color] = color;
                colorCount++;
            }

            count += 4;
        }

        console.log(colorCount);
    };

    return MapProvinceReader;
});

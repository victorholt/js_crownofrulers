define(function(require) {
    'use strict';

    var THREE = require('threejs');
    var $ = require('jquery');

    /**
     * Handles methods for creating and rendering the frustum.
     *
     * @class Frustum
     * @constructor
     */
    var Frustum = function(camera)
    {
        /**
         * The camera used to generate the frustum.
         *
         * @property camera
         * @type {Camera}
         */
        this.camera = camera;

        /**
         * The view frustum object.
         *
         * @property frustum
         * @type {THREE<Frustum>}
         */
        this.frustum = null;

        /**
         * Tracks whether or not our graphics driver is enabled.
         *
         * @property isEnabled
         * @type {Boolean}
         */
        this.isEnabled = false;

        /**
         * Matrix of the frustum.
         *
         * @property mat4
         * @type {THREE.Matrix4}
         */
        this.mat4 = new THREE.Matrix4();

        this.init();
    };
    var proto = Frustum.prototype;

    /**
     * Initializes the class.
     *
     * @method init
     * @chainable
     */
    proto.init = function() {
        var camera = this.camera.getCameraContext();

        this.frustum = new THREE.Frustum();
        //this.frustum.setFromMatrix(new THREE.Matrix4().multiply(camera.projectionMatrix, camera.matrixWorldInverse));
        this.frustum.setFromMatrix(new THREE.Matrix4().multiplyMatrices(camera.matrixWorldInverse,
                                                                            camera.projectionMatrix));

        this.setupEventHandlers()
            .setupChildren()
            .enable();

        return this;
    };

    /**
     * Links up the handlers to their events.
     *
     * @method setupEventHandlers
     * @chainable
     * @private
     */
    proto.setupEventHandlers = function() {
        return this;
    };

    /**
     * Creates/assigns the children objects
     * of the component.
     *
     * @method setupChildren
     * @chainable
     * @private
     */
    proto.setupChildren = function() {
        this.$window = $(window);

        return this;
    };

    /**
     * Enables the class and events.
     *
     * @method enable
     * @chainable
     */
    proto.enable = function() {
        if (this.isEnabled) {
            return this;
        }

        this.isEnabled = true;

        return this;
    };

    /**
     * Disables the class and events.
     *
     * @method disable
     * @chainable
     */
    proto.disable = function() {
        if (!this.isEnabled) {
            return this;
        }

        this.isEnabled = false;

        return this;
    };

    /**
     * Updates the frustum.
     *
     * @method update
     * @chainable
     */
    proto.update = function() {
        if (!this.isEnabled) {
            return this;
        }

        var camera = this.camera.getCameraContext();

        this.mat4.identity();
        this.frustum.setFromMatrix(this.mat4.multiplyMatrices(camera.projectionMatrix,
                                                              camera.matrixWorldInverse));

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the THREE frustum context.
     *
     * @method getFrustumContext
     * @returns {THREE.Frustum}
     */
    proto.getFrustumContext = function() {
        return this.frustum;
    };

    return Frustum;
});

define(function(require) {
    'use strict';

    var UIElement = require('./uiElement');
    var $ = require('jquery');

    /**
     * Displays the stats for the graphics.
     *
     * @class GraphicsStatsPanel
     * @constructor
     */
    var GraphicsStatsPanel = function(gameEngine) {
        /**
         * The game engine.
         *
         * @property gameEngine
         * @type {App}
         */
        this.gameEngine = gameEngine;

        this.init();
    };

    // Inherit the UIElement class.
    GraphicsStatsPanel.prototype = Object.create(UIElement.prototype);
    var proto = GraphicsStatsPanel.prototype;

    /**
     * Initializes the UI.
     *
     * @method init
     * @chainable
     */
    proto.init = function() {
        var settings = this.gameEngine.getSettings();
        var $gameElement = this.gameEngine.getGameElement();

        $gameElement.append('<div class="gameStats-graphics"></div>');
        $('.gameStats-graphics').html(
            '<ul>' +
            '<li class="gameStats-graphics-verticesRendered">' +
            'Vertices Rendered: ' + settings.totalVerticesRendered +
            '</li>' +
            '</ul>'
        );

        this.setElement($('.gameStats-graphics'));

        if (settings.showDebugInfo) {
            this.$element.hide();
        }

        return this;
    };

    /**
     * Updates the panel with new information.
     *
     * @method update
     * @chainable
     */
    proto.update = function() {
        var settings = this.gameEngine.getSettings();
        $('.gameStats-graphics-verticesRendered').html(
            'Vertices Rendered: ' + settings.totalVerticesRendered
        );

        return this;
    };

    return GraphicsStatsPanel;
});

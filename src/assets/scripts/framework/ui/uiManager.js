define(function(require) {
    'use strict';

    var GraphicsStatsPanel = require('./graphicsStatsPanel');

    /**
     * Handles managing the collection of UI elements
     * on the screen.
     *
     * @class UIManager
     * @constructor
     */
    var UIManager = function(gameEngine) {
        /**
         * The game engine.
         *
         * @property gameEngine
         * @type {App}
         */
        this.gameEngine = gameEngine;

        /**
         * Array of UI elements loaded into the game.
         *
         * @property uiElements
         * @type {Array<UIElement>}
         */
        this.uiElements = [];

        this.init();
    };
    var proto = UIManager.prototype;

    /**
     * Initializes the UI manager.
     *
     * @method init
     * @chainable
     */
    proto.init = function() {
        this.uiElements.push(new GraphicsStatsPanel(this.gameEngine));

        return this;
    };

    /**
     * Updates all UI elements.
     *
     * @method update
     * @chainable
     */
    proto.update = function() {
        if (this.uiElements.length === 0) {
            return this;
        }

        var i = 0;
        for (; i < this.uiElements.length; i++) {
            this.uiElements[i].update();
        }

        return this;
    };

    return UIManager;
});

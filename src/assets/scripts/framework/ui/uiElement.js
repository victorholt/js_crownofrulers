define(function(require) {
    'use strict';

    /**
     * The base UI element class.
     *
     * @class UIElement
     * @constructor
     */
    var UIElement = function() {
        /**
         * The reference to the jQuery HTMLObject.
         *
         * @property $element
         * @type {jQuery<HTMLObject>}
         */
        this.$element = null;
    };
    var proto = UIElement.prototype;

    ///////////////////////////////////////////////////////////////////////////
    // Helpers
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Hides the element.
     *
     * @method hideElement
     * @chainable
     */
    proto.hideElement = function() {
        if (this.$element == null) {
            return this;
        }

        this.$element.hide();

        return this;
    };

    /**
     * Shows the element.
     *
     * @method showElement
     * @chainable
     */
    proto.showElement = function() {
        if (this.$element == null) {
            return this;
        }

        this.$element.show();

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the reference to the jQuery HTMLObject.
     *
     * @method getElement
     * @returns {jQuery<HTMLElement>}
     */
    proto.getElement = function() {
        return this.$element;
    };

    /**
     * Sets the reference to the jQuery HTMLObject.
     *
     * @method setElement
     */
    proto.setElement = function($element) {
        this.$element = $element;
    };

    return UIElement;
});

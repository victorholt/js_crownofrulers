define(function(require) {
    'use strict';

    var THREE = require('threejs');
    var KeyInput = require('./keyInput');
    var $ = require('jquery');

    // List of specific input states.
    var STATE = {
        NONE: -1,
        ROTATE: 0,
        ZOOM: 1,
        PAN: 2,
        TOUCH_ROTATE: 3,
        TOUCH_ZOOM_PAN: 4
    };

    var KEYS = [
        KeyInput.KEY_A,
        KeyInput.KEY_S,
        KeyInput.KEY_D
    ];

    /**
     * Handles trackball controls for the camera object.
     *
     * @class CameraTrackballControls
     * @constructor.
     */
    var CameraTrackballControls = function(camera, $element) {
        /**
         * The perspective camera object.
         *
         * @property camera
         * @type {Three<PerspectiveCamera}
         */
        this.camera = camera;

        /**
         * The DOM reference for rendering.
         *
         * @property $element
         * @type {jQuery<HTMLElement>}
         */
        this.$element = ($element !== undefined) ? $element : document;

        /**
         * The screen width/height/top/left information.
         *
         * @property screen
         * @type {Object}
         */
        this.screen = {
            left: 0,
            top: 0,
            width: 0,
            height: 0
        };

        /**
         * Camera rotation speed.
         *
         * @property rotationSpeed
         * @type {double}
         */
        this.rotationSpeed = 1.0;

        /**
         * Camera zoom in/out speed.
         *
         * @property zoomSpeed
         * @type {double}
         */
        this.zoomSpeed = 1.0;

        /**
         * Camera pan speed.
         *
         * @property panSpeed
         * @type {double}
         */
        this.panSpeed = 0.3;

        /**
         * The camera dynamic damping factor.
         *
         * @property dynamicDampingFactor
         * @type {double}
         */
        this.dynamicDampingFactor = 0.2;

        /**
         * The minimal distance of the camera.
         *
         * @property minDistance
         * @type {int}
         */
        this.minDistance = 0;

        /**
         * The maximum distance of the camera.
         *
         * @property maxDistance
         * @type {Infinity}
         */
        this.maxDistance = Infinity;

        /**
         * The focus target of the camera.
         *
         * @property cameraTarget
         * @type {THREE.Vector3}
         */
        this.cameraTarget = new THREE.Vector3();

        /**
         * The last camera position.
         *
         * @property lastPosition
         * @type {THREE.Vector3}
         */
        this.lastPosition = new THREE.Vector3();

        /**
         * The lookAt camera vector.
         *
         * @property lookAt
         * @type {THREE.Vector3}
         */
        this.lookAt = new THREE.Vector3();

        /**
         * The starting vector of the camera rotation.
         *
         * @property rotateStart
         * @type {THREE.Vector3}
         */
        this.rotateStart = new THREE.Vector3();

        /**
         * The end vector of the camera rotation.
         *
         * @property rotateEnd
         * @type {THREE.Vector3}
         */
        this.rotateEnd = new THREE.Vector3();

        /**
         * The starting vector of the camera zoom.
         *
         * @property zoomStart
         * @type {THREE.Vector3}
         */
        this.zoomStart = new THREE.Vector3();

        /**
         * The end vector of the camera zoom.
         *
         * @property zoomEnd
         * @type {THREE.Vector3}
         */
        this.zoomEnd = new THREE.Vector3();

        /**
         * The starting vector of the camera touch zoom.
         *
         * @property touchZoomDistanceStart
         * @type {THREE.Vector3}
         */
        this.touchZoomDistanceStart = new THREE.Vector3();

        /**
         * The end vector of the camera touch zoom.
         *
         * @property touchZoomDistanceEnd
         * @type {THREE.Vector3}
         */
        this.touchZoomDistanceEnd = new THREE.Vector3();

        /**
         * The starting vector of the camera pan.
         *
         * @property panStart
         * @type {THREE.Vector3}
         */
        this.panStart = new THREE.Vector3();

        /**
         * The end vector of the camera pan.
         *
         * @property panEnd
         * @type {THREE.Vector3}
         */
        this.panEnd = new THREE.Vector3();

        /**
         * The current state of the camera.
         *
         * @property currentState
         * @type {int<STATE>}
         */
        this.currentState = STATE.NONE;

        /**
         * The previous state of the camera.
         *
         * @property prevState
         * @type {int<STATE>}
         */
        this.prevState = STATE.NONE;

        /**
         * The error distance threshold.
         *
         * @property errorThreshold
         * @type {errorThreshold}
         */
        this.errorThreshold = 0.000001;

        /**
         * Flag to check if camera rotation is enabled.
         *
         * @property enableRotation
         * @type {Boolean}
         */
        this.enableRotation = false;

        /**
         * Flag to check if camera zooming is enabled.
         *
         * @property enableZoom
         * @type {Boolean}
         */
        this.enableZoom = false;

        /**
         * Flag to check if camera panning is enabled.
         *
         * @property enablePan
         * @type {Boolean}
         */
        this.enablePan = false;

        /**
         * Flag to check if camera rolling is enabled.
         *
         * @property enableRoll
         * @type {Boolean}
         */
        this.enableRoll = false;

        /**
         * Flag to check if camera movement is static.
         *
         * @property staticMoving
         * @type {Boolean}
         */
        this.staticMoving = false;

        /**
         * Flag that checks whether or not this component
         * is currently enabled.
         *
         * @property isEnabled
         * @type Boolean
         */
        this.isEnabled = false;

        this.init();
    };

    var proto = CameraTrackballControls.prototype;

    /**
     * Initializes all the view. This method should setup all
     * event handlers, children and layout manipulations. Once
     * everything has been setup it will promptly enable the
     * component.
     *
     * @method init
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.init = function() {
        this.setupEventHandlers()
            .setupChildren()
            .enable()
            .handleResize()
            .update();

        return this;
    };

    /**
     * Links up the handlers to their events.
     *
     * @method setupEventHandlers
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.setupEventHandlers = function() {
        this.changeEventHandler = this.onChangeEvent.bind(this);
        this.startEventHandler = this.onStartEvent.bind(this);
        this.endEventHandler = this.onEndEvent.bind(this);
        this.resizeEventHandler = this.onResizeEvent.bind(this);
        this.keyDownEventHandler = this.onKeyDownEvent.bind(this);
        this.keyUpEventHandler = this.onKeyUpEvent.bind(this);
        this.mouseDownEventHandler = this.onMouseDownEvent.bind(this);
        this.mouseMoveEventHandler = this.onMouseMoveEvent.bind(this);
        this.mouseUpEventHandler = this.onMouseUpEvent.bind(this);
        this.mouseWheelEventHandler = this.onMouseWheelEvent.bind(this);
        this.touchStartEventHandler = this.onTouchStartEvent.bind(this);
        this.touchMoveEventHandler = this.onTouchMoveEvent.bind(this);
        this.touchEndEventHandler = this.onTouchEndEvent.bind(this);

        return this;
    };

    /**
     * Creates/assigns the children objects
     * of the component.
     *
     * @method setupChildren
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.setupChildren = function() {
        this.$window = $(window);

        this.initCameraTarget = this.cameraTarget.clone();
        this.initPosition = this.camera.position.clone();
        this.initUp = this.camera.up.clone();

        return this;
    };

    /**
     * Removes the children objects of the component.
     *
     * @method removeChildren
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.removeChildren = function() {
        return this;
    };

    /**
     * Enables the component and all of the events.
     *
     * @method enable
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.enable = function() {
        if (this.isEnabled) {
            return this;
        }

        // Enable the events.
        this.$window.on('resize', this.resizeEventHandler)
                    .on('inputKeyDown', this.keyDownEventHandler)
                    .on('inputKeyUp', this.keyUpEventHandler)
                    .on('mousedown', this.mouseDownEventHandler)
                    .on('mousemove', this.mouseMoveEventHandler)
                    .on('mouseup', this.mouseUpEventHandler)
                    .on('DOMMouseScroll', this.mouseWheelEventHandler)
                    .on('touchstart', this.touchStartEventHandler)
                    .on('touchend', this.touchEndEventHandler)
                    .on('touchmove', this.touchMoveEventHandler)
                    .on('changeCamera', this.changeEventHandler)
                    .on('startCamera', this.startEventHandler)
                    .on('endCamera', this.endEventHandler);

        // Enable the camera.
        this.enableRotation = true;
        this.enableZoom = true;
        this.enablePan = true;
        this.enableRoll = true;
        this.isEnabled = true;

        return this;
    };

    /**
     * Disables the component and all events associated
     * with the component.
     *
     * @method disable
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.disable = function() {
        if (!this.isEnabled) {
            return this;
        }

        // Disable the events.
        this.$window.off('resize', this.resizeEventHandler)
                    .off('inputKeyDown', this.keyDownEventHandler)
                    .off('inputKeyUp', this.keyUpEventHandler)
                    .off('mousedown', this.mouseDownEventHandler)
                    .off('mousemove', this.mouseMoveEventHandler)
                    .off('mouseup', this.mouseUpEventHandler)
                    .off('DOMMouseScroll', this.mouseWheelEventHandler)
                    .off('touchstart', this.touchStartEventHandler)
                    .off('touchend', this.touchEndEventHandler)
                    .off('touchmove', this.touchMoveEventHandler)
                    .off('changeCamera', this.changeEventHandler)
                    .off('startCamera', this.startEventHandler)
                    .off('endCamera', this.endEventHandler);

        this.enableRotation = false;
        this.enableZoom = false;
        this.enablePan = false;
        this.enableRoll = false;
        this.isEnabled = false;
        return this;
    };

    /**
     * Disables/removes all children objects created/assigned
     * by the component.
     *
     * @method destroy
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.destroy = function() {
        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // EVENTS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Handles the change event for the camera state.
     *
     * @method onChangeEvent
     */
    proto.onChangeEvent = function(event) {
        event.cameraEventType = 'change';
    };

    /**
     * Handles the start event for the camera state.
     *
     * @method onStartEvent
     */
    proto.onStartEvent = function(event) {
        event.cameraEventType = 'start';
    };

    /**
     * Handles the end event for the camera state.
     *
     * @method onEndEvent
     */
    proto.onEndEvent = function(event) {
        event.cameraEventType = 'end';
    };

    /**
     * Handles the resize event.
     *
     * @method onResizeEvent
     */
    proto.onResizeEvent = function() {
        this.handleResize();
    };

    /**
     * Handles the keydown event.
     *
     * @method onKeyDownEvent
     */
    proto.onKeyDownEvent = function(event, keyCode) {
        this.prevState = this.state;

        if (this.state !== STATE.NONE) {
            return;
        } else if (keyCode === KEYS[STATE.ROTATE] && this.enableRotation) {
            this.state = STATE.ROTATE;
        } else if (keyCode === KEYS[STATE.ZOOM] && this.enableZoom) {
            this.state = STATE.ZOOM;
        } else if (keyCode === KEYS[STATE.PAN] && this.enablePan) {
            this.state = STATE.PAN;
        }
    };

    /**
     * Handle the keyup event.
     *
     * @method onKeyUpEvent
     */
    proto.onKeyUpEvent = function(event, keyCode) {
        this.state = this.prevState;
    };

    /**
     * Handles the mouse down event.
     *
     * @method onMouseDownEvent
     */
    proto.onMouseDownEvent = function(event) {
        event.preventDefault();
        event.stopPropagation();

        if (this.state === STATE.NONE) {
            this.state = event.button;
        }

        if (this.state === STATE.ROTATE && this.enableRotation) {
            this.rotateStart.copy( this.getMouseProjectionOnBall(event.pageX, event.pageY));
            this.rotateEnd.copy(this.rotateStart);
        } else if (this.state === STATE.ZOOM && this.enableZoom) {
            this.zoomStart.copy(this.getMouseOnScreen(event.pageX, event.pageY));
            this.zoomEnd.copy(this.zoomStart);
        } else if (this.state === STATE.PAN && this.enablePan) {
            this.panStart.copy(this.getMouseOnScreen(event.pageX, event.pageY));
            this.panEnd.copy(this.panStart);
        }

        //document.addEventListener('mousemove', mousemove, false);
        //document.addEventListener('mouseup', mouseup, false);

        this.$window.trigger('startCamera');
    };

    /**
     * Handles the mouse moving event.
     *
     * @method onMouseMoveEvent
     */
    proto.onMouseMoveEvent = function(event) {
        event.preventDefault();
        event.stopPropagation();

        if (this.state === STATE.ROTATE && this.enableRotation) {
            this.rotateEnd.copy(this.getMouseProjectionOnBall(event.pageX, event.pageY));
        } else if (this.state === STATE.ZOOM && this.enableZoom) {
            this.zoomEnd.copy(this.getMouseOnScreen(event.pageX, event.pageY));
        } else if (this.state === STATE.PAN && this.enablePan) {
            this.panEnd.copy(this.getMouseOnScreen(event.pageX, event.pageY));
        }
    };

    /**
     * Handles the mouse up event.
     *
     * @method onMouseUpEvent
     */
    proto.onMouseUpEvent = function(event) {
        event.preventDefault();
        event.stopPropagation();

        this.state = STATE.NONE;

        //document.removeEventListener( 'mousemove', mousemove );
        //document.removeEventListener( 'mouseup', mouseup );
        this.$window.trigger('endCamera');
    };

    /**
     * Handles hte mouse wheel event.
     *
     * @method onMouseWheelEvent
     */
    proto.onMouseWheelEvent = function(event) {
        event.preventDefault();
        event.stopPropagation();

        var delta = 0;

        if (event.wheelDelta) { // WebKit / Opera / Explorer 9
            delta = event.wheelDelta / 40;
        } else if (event.detail) { // Firefox
            delta = - event.detail / 3;
        }

        this.zoomStart.y += delta * 0.01;
        this.$window.trigger('startCamera');
        this.$window.trigger('endCamera');
    };

    /**
     * Handles the touch start event.
     *
     * @method onTouchStartEvent
     */
    proto.onTouchStartEvent = function(event) {
        switch (event.touches.length) {
            case 1:
                this.state = STATE.TOUCH_ROTATE;
                this.rotateStart.copy(this.getMouseProjectionOnBall(event.touches[0].pageX, event.touches[0].pageY));
                this.rotateEnd.copy(this.rotateStart);
                break;
            case 2:
                this.state = STATE.TOUCH_ZOOM_PAN;
                var dx = event.touches[0].pageX - event.touches[1].pageX;
                var dy = event.touches[0].pageY - event.touches[1].pageY;
                this.touchZoomDistanceEnd = this.touchZoomDistanceStart = Math.sqrt(dx * dx + dy * dy);

                var x = (event.touches[0].pageX + event.touches[1].pageX) / 2;
                var y = (event.touches[0].pageY + event.touches[1].pageY) / 2;
                this.panStart.copy(this.getMouseOnScreen(x, y));
                this.panEnd.copy(this.panStart);
                break;
            default:
                this.state = STATE.NONE;
        }
        this.$window.trigger('startCamera');
    };

    /**
     * Handles the touch move event.
     *
     * @method onTouchMoveEvent
     */
    proto.onTouchMoveEvent = function(event) {
        event.preventDefault();
        event.stopPropagation();

        switch (event.touches.length) {
            case 1:
                this.rotateEnd.copy(this.getMouseProjectionOnBall(event.touches[0].pageX, event.touches[0].pageY));
                break;
            case 2:
                var dx = event.touches[0].pageX - event.touches[1].pageX;
                var dy = event.touches[0].pageY - event.touches[1].pageY;
                this.touchZoomDistanceEnd = Math.sqrt(dx * dx + dy * dy);

                var x = (event.touches[0].pageX + event.touches[1].pageX) / 2;
                var y = (event.touches[0].pageY + event.touches[1].pageY) / 2;
                this.panEnd.copy(this.getMouseOnScreen(x, y));
                break;
            default:
                this.state = STATE.NONE;
        }
    };

    /**
     * Handles the touch end event.
     *
     * @method onTouchEndEvent
     */
    proto.onTouchEndEvent = function(event) {
        switch (event.touches.length) {
            case 1:
                this.rotateEnd.copy(this.getMouseProjectionOnBall(event.touches[0].pageX, event.touches[0].pageY));
                this.rotateStart.copy(this.rotateEnd);
                break;

            case 2:
                this.touchZoomDistanceStart = this.touchZoomDistanceEnd = 0;

                var x = (event.touches[0].pageX + event.touches[1].pageX) / 2;
                var y = (event.touches[0].pageY + event.touches[1].pageY) / 2;
                this.panEnd.copy(this.getMouseOnScreen(x, y));
                this.panStart.copy(this.panEnd);
                break;
        }

        this.state = STATE.NONE;
        this.$window.trigger('endCamera');
    };

    ///////////////////////////////////////////////////////////////////////////
    // GETTERS/SETTERS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Retrieves the mouse on screen as a vector.
     *
     * @method getMouseOnScreen
     * @param {int} pageX
     * @param {int} pageY
     * @returns {THREE.Vector2}
     */
    proto.getMouseOnScreen = function(pageX, pageY) {
        var vector = new THREE.Vector2();

        vector.set(
            (pageX - this.screen.left) / this.screen.width,
            (pageY - this.screen.top) / this.screen.height
        );

        return vector;
    };

    /**
     * Retrieves the mouse projects on a point.
     *
     * @method getMouseProjectionOnBall
     * @param {int} pageX
     * @param {int} pageY
     * @returns {THREE.Vector3}
     */
    proto.getMouseProjectionOnBall = function(pageX, pageY) {
        var vector = new THREE.Vector3();
        var objectUp = new THREE.Vector3();
        var mouseOnBall = new THREE.Vector3();

        mouseOnBall.set(
            (pageX - this.screen.width * 0.5 - this.screen.left) / (this.screen.width * 0.5),
            (this.screen.height * 0.5 + this.screen.top - pageY) / (this.screen.height * 0.5),
            0.0
        );

        var length = mouseOnBall.length();

        if (this.noRoll) {
            if (length < Math.SQRT1_2) {
                mouseOnBall.z = Math.sqrt( 1.0 - length*length );
            } else {
                mouseOnBall.z = 0.5 / length;
            }

        } else if (length > 1.0) {
            mouseOnBall.normalize();
        } else {
            mouseOnBall.z = Math.sqrt( 1.0 - length * length );
        }

        this.lookAt.copy(this.camera.position).sub(this.cameraTarget);

        vector.copy(this.camera.up).setLength(mouseOnBall.y);
        vector.add(objectUp.copy(this.camera.up).cross(this.lookAt).setLength(mouseOnBall.x));
        vector.add(this.lookAt.setLength(mouseOnBall.z));

        return vector;
    };

    ///////////////////////////////////////////////////////////////////////////
    // HELPERS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Handles the setup of the camera when the window
     * has been resized.
     *
     * @method handleResize
     * @returns {CameraTrackballControls}
     */
    proto.handleResize = function() {
        if (this.$element === document) {
            this.screen.left = 0;
            this.screen.top = 0;
            this.screen.width = window.innerWidth;
            this.screen.height = window.innerHeight;
        } else {
            var box = this.$element.getBoundingClientRect();
            // adjustments come from similar code in the jquery offset() function
            var d = this.$element.ownerDocument.documentElement;
            this.screen.left = box.left + window.pageXOffset - d.clientLeft;
            this.screen.top = box.top + window.pageYOffset - d.clientTop;
            this.screen.width = box.width;
            this.screen.height = box.height;
        }

        return this;
    };

    /**
     * Handles rotating the camera.
     *
     * @method rotateCamera
     * @returns {CameraTrackballControls}
     */
    proto.rotateCamera = function() {
        if (!this.enableRotation) {
            return this;
        }

        var axis = new THREE.Vector3();
        var quaternion = new THREE.Quaternion();
        var angle = Math.acos(
                        this.rotateStart.dot(this.rotateEnd) / this.rotateStart.length() / this.rotateEnd.length()
                    );

        if (angle) {
            axis.crossVectors(this.rotateStart, this.rotateEnd).normalize();

            angle *= this.rotateSpeed;

            quaternion.setFromAxisAngle( axis, -angle );

            this.lookAt.applyQuaternion(quaternion);
            this.camera.up.applyQuaternion(quaternion);

            this.rotateEnd.applyQuaternion(quaternion);

            if (this.staticMoving) {
                this.rotateStart.copy(this.rotateEnd);
            } else {
                quaternion.setFromAxisAngle(axis, angle * (this.dynamicDampingFactor - 1.0));
                this.rotateStart.applyQuaternion(quaternion);
            }
        }

        return this;
    };

    /**
     * Handles zooming with the camera.
     *
     * @method zoomCamera
     * @returns {CameraTrackballControls}
     */
    proto.zoomCamera = function() {
        var factor = 0;

        if (this.currentState === STATE.TOUCH_ZOOM_PAN) {
            factor = this.touchZoomDistanceStart / this.touchZoomDistanceEnd;
            this.touchZoomDistanceStart = this.touchZoomDistanceEnd;
            this.lookAt.multiplyScalar(factor);
        } else {
            factor = 1.0 + (this.zoomEnd.y - this.zoomStart.y) * this.zoomSpeed;

            if (factor !== 1.0 && factor > 0.0) {
                this.lookAt.multiplyScalar(factor);

                if (this.staticMoving) {
                    this.zoomStart.copy(this.zoomEnd);
                } else {
                    this.zoomStart.y += (this.zoomEnd.y - this.zoomStart.y) * this.dynamicDampingFactor;
                }

            }

        }
    };

    /**
      * Handles panning with the camera.
      *
      * @method panCamera
      * @returns {CameraTrackballControls}
      */
    proto.panCamera = function() {
        var mouseChange = new THREE.Vector2();
        var objectUp = new THREE.Vector3();
        var pan = new THREE.Vector3();

        mouseChange.copy( this.panEnd ).sub( this.panStart );

        if (mouseChange.lengthSq()) {
            mouseChange.multiplyScalar(this.lookAt.length() * this.panSpeed);

            pan.copy(this.lookAt ).cross(this.camera.up).setLength(mouseChange.x);
            pan.add(objectUp.copy(this.camera.up).setLength(mouseChange.y));

            this.camera.position.add(pan);
            this.cameraTarget.add(pan);

            if (this.staticMoving) {
                this.panStart.copy(this.panEnd);
            } else {
                this.panStart.add(
                    mouseChange.subVectors(this.panEnd, this.panStart).multiplyScalar(this.dynamicDampingFactor)
                );
            }
        }

        return this;
    };

    /**
     * Check the min/max distance of the camera position.
     *
     * @method checkDistances
     * @returns {CameraTrackballControls}
     */
    proto.checkDistances = function() {
        if (!this.noZoom || !this.noPan) {
            if (this.lookAt.lengthSq() > this.maxDistance * this.maxDistance) {
                this.camera.position.addVectors(this.cameraTarget, this.lookAt.setLength(this.maxDistance));
            }

            if (this.lookAt.lengthSq() < this.minDistance * this.minDistance ) {
                this.camera.position.addVectors(this.cameraTarget, this.lookAt.setLength(this.minDistance));
            }
        }

        return this;
    };

    /**
     * Handles updating the camera.
     *
     * @method update
     * @returns {CameraTrackballControls}
     */
    proto.update = function() {
        this.lookAt.subVectors( this.camera.position, this.cameraTarget );

        if (this.enableRotation) {
            this.rotateCamera();
        }

        if (this.enableZoom) {
            this.zoomCamera();
        }

        if (this.enablePan) {
            this.panCamera();
        }

        this.camera.position.addVectors( this.cameraTarget, this.lookAt );
        this.checkDistances();
        this.camera.lookAt( this.cameraTarget );

        if (this.lastPosition.distanceToSquared(this.camera.position) > this.errorThreshold) {
            this.$window.trigger('changeCamera');
            this.lastPosition.copy(this.camera.position);
        }

        return this;
    };

    /**
     * Resets the camera state/position.
     *
     * @method reset
     * @returns {CameraTrackballControls}
     */
    proto.reset = function() {
        this.state = STATE.NONE;
        this.prevState = STATE.NONE;

        this.cameraTarget.copy(this.initCameraTarget);
        this.camera.position.copy(this.initPosition);
        this.camera.up.copy(this.initUp);

        this.lookAt.subVectors(this.camera.position, this.cameraTarget);

        this.camera.lookAt(this.cameraTarget);

        this.$window.trigger('changeCamera');
        this.lastPosition.copy(this.camera.position);

        return this;
    };

    return CameraTrackballControls;
});

define(function(require) {
    'use strict';

    var $ = require('jquery');

    /**
     * This class handles keyboard input events.
     *
     * @class KeyInput
     * @constructor
     */
    var KeyInput = function() {
        /**
         * Holds the available key states.
         *
         * @property keyState
         * @type {Object}
         */
        this.keyState = {};

        /**
         * Flag that checks whether or not this component
         * is currently enabled.
         *
         * @property isEnabled
         * @type Boolean
         */
        this.isEnabled = false;

        this.init();
    };

    var proto = KeyInput.prototype;

    /**
     * Key Inputs
     */
    KeyInput.KEY_A = 65;
    KeyInput.KEY_D = 68;
    KeyInput.KEY_S = 83;
    KeyInput.KEY_W = 87;

    /**
     * Initializes all the view. This method should setup all
     * event handlers, children and layout manipulations. Once
     * everything has been setup it will promptly enable the
     * component.
     *
     * @method init
     * @returns {KeyInput}
     * @private
     */
    proto.init = function() {
        this.reset()
            .setupEventHandlers()
            .setupChildren()
            .enable();

        return this;
    };

    /**
     * Links up the handlers to their events.
     *
     * @method setupEventHandlers
     * @returns {KeyInput}
     * @private
     */
    proto.setupEventHandlers = function() {
        this.keyDownEventHandler = this.onKeyDownEvent.bind(this);
        this.keyUpEventHandler = this.onKeyUpEvent.bind(this);
        this.keyBlurEventHandler = this.onKeyBlurEvent.bind(this);

        return this;
    };

    /**
     * Creates/assigns the children objects
     * of the component.
     *
     * @method setupChildren
     * @returns {KeyInput}
     * @private
     */
    proto.setupChildren = function() {
        this.$window = $(window);
        this.$document = $(document);

        return this;
    };

    /**
     * Removes the children objects of the component.
     *
     * @method removeChildren
     * @returns {KeyInput}
     * @private
     */
    proto.removeChildren = function() {
        return this;
    };

    /**
     * Enables the component and all of the events.
     *
     * @method enable
     * @returns {KeyInput}
     */
    proto.enable = function() {
        if (this.isEnabled) {
            return this;
        }

        this.$document.on('keydown', this.keyDownEventHandler)
                      .on('keyup', this.keyUpEventHandler);

        this.$window.on('blur', this.keyBlurEventHandler);

        this.isEnabled = true;
        return this;
    };

    /**
     * Disables the component and all events associated
     * with the component.
     *
     * @method disable
     * @returns {KeyInput}
     */
    proto.disable = function() {
        if (!this.isEnabled) {
            return this;
        }

        this.$document.off('keydown', this.keyDownEventHandler)
                      .off('keyup', this.keyUpEventHandler);

        this.$window.off('blur', this.keyBlurEventHandler);

        this.isEnabled = false;
        return this;
    };

    /**
     * Disables/removes all children objects created/assigned
     * by the component.
     *
     * @method destroy
     * @returns {KeyInput}
     * @private
     */
    proto.destroy = function() {
        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // EVENTS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Handles the keydown event.
     *
     * @method onKeyDownEvent
     * @param {jQuery<Event>}
     */
    proto.onKeyDownEvent = function(event) {
        var key = this.keyState[event.keyCode];

        if (key != null) {
            this.keyState[event.keyCode] = true;
            this.$window.trigger('inputKeyDown', [event.keyCode]);

            event.preventDefault();
            return false;
        }
    };

    /**
     * Handles the keyup event.
     *
     * @method onKeyUpEvent
     * @param {jQuery<Event>}
     */
    proto.onKeyUpEvent = function(event) {
        var key = this.keyState[event.keyCode];

        if (key != null) {
            this.keyState[event.keyCode] = false;
            this.$window.trigger('inputKeyUp', [event.keyCode]);

            event.preventDefault();
            return false;
        }
    };

    /**
     * Handles the blur event.
     *
     * @method onKeyBlurEvent
     * @param {jQuery<Event>}
     */
    proto.onKeyBlurEvent = function(event) {
        this.reset();
        this.$window.trigger('inputKeyReset');
    };

    ///////////////////////////////////////////////////////////////////////////
    // HELPERS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Resets the key states.
     *
     * @method reset
     * @returns {KeyInput}
     */
    proto.reset = function() {
        this.keyState[KeyInput.KEY_A] = true;
        this.keyState[KeyInput.KEY_D] = true;
        this.keyState[KeyInput.KEY_S] = true;
        this.keyState[KeyInput.KEY_W] = true;

        return this;
    };

    return KeyInput;
});

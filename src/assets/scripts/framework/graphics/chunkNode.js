define(function(require) {
    'use strict';

    var THREE = require('threejs');
    var terrainVertShader = require('shader!terrain.vert');
    var terrainFragShader = require('shader!terrain.frag');
    var logger = require('logger');

    /**
     * Chunk properties for the node.
     *
     * @class ChunkProps
     * @structure
     */
    var ChunkProps = function(props) {
        this.parent = props.parent != null ? props.parent : null;
        this.geometry = null;
        this.min = props.min;
        this.max = props.max;
        this.depth = props.depth;
        this.size = props.size;
        this.offset = props.offset;
        this.color = props.color != null ? props.color : 0xFFFFFF;
        this.scale = props.scale != null ? props.scale : 1;
    };

    /**
     * Handles building and testing the quadtree.
     *
     * @class ChunkNode
     * @constructor
     * @see http://tulrich.com/geekstuff/sig-notes.pdf, http://charpie.fr/
     */
    var ChunkNode = function(sceneObject, nodeList, props, gameEngine, heightmap) {
        /**
         * The game engine.
         *
         * @property gameEngine
         * @type {App}
         */
        this.gameEngine = gameEngine;

        /**
         * The graphics driver.
         *
         * @property graphicsDriver
         * @type {GraphicsDriver}
         */
        this.graphicsDriver = gameEngine.getGraphicsDriver();

        /**
         * The heightmap for the terrain.
         *
         * @property heightmap
         * @type {Heightmap}
         */
        this.heightmap = heightmap;

        /**
         * The properties of the ChunkNode.
         *
         * @property props
         * @type {ChunkProps}
         */
        this.props = new ChunkProps(props);

        /**
         * The terrain chunk of the quadtree.
         *
         * @property chunk
         * @type {Array}
         */
        this.chunk = null;

        /**
         * The quadtree children nodes.
         *
         * @property children
         * @type {Array<ChunkNode>}
         */
        this.children = [];

        /**
         * The scene object.
         *
         * @property sceneObject
         * @type {THREE<Object3D>}
         */
        this.sceneObject = sceneObject;

        /**
         * Tracks whether or not the chunk data has been loaded
         * into the geometry objects created.
         *
         * @property isChunkDataLoaded
         * @type {boolean}
         */
        this.isChunkDataLoaded = false;

        /**
         * Tracks whether or not the vertex normals
         * for the chunk has been computed.
         *
         * @property isNormalsComputed
         * @type {boolean}
         */
        this.isNormalsComputed = false;

        this.init(nodeList);
    };
    var proto = ChunkNode.prototype;

    /**
     * Initializes the quadtree.
     *
     * @method init
     * @param Array nodeList
     * @chainable
     */
    proto.init = function(nodeList) {
        var depth = this.props.depth;
        var resolution = this.props.size;

        this.lod = Math.pow(2, depth) * this.graphicsDriver.getSettings().terrainViewport;

        this.loadChunkData();

        if (depth > 0) {
            depth--;

            var xMid = (this.props.min.x + this.props.max.x)/2;
            var yMid = (this.props.min.y + this.props.max.y)/2;
            var size = resolution/2;
            var offsetX = this.props.offset.x;
            var offsetY = this.props.offset.y;

            var propsNode1 = {
                parent: this,
                min: new THREE.Vector2(this.props.min.x, this.props.min.y),
                max: new THREE.Vector2(xMid, yMid),
                depth: depth,
                size: size,
                offset: new THREE.Vector2(offsetX, offsetY),
                color: '0x'+Math.floor(Math.random()*16777215).toString(16),
                scale: this.props.scale
            };

            var propsNode2 = {
                parent: this,
                min: new THREE.Vector2(this.props.min.x, yMid),
                max: new THREE.Vector2(xMid, this.props.max.y),
                depth: depth,
                size: size,
                offset: new THREE.Vector2(offsetX, offsetY + size),
                color: '0x'+Math.floor(Math.random()*16777215).toString(16),
                scale: this.props.scale
            };

            var propsNode3 = {
                parent: this,
                min: new THREE.Vector2(xMid, yMid),
                max: new THREE.Vector2(this.props.max.x, this.props.max.y),
                depth: depth,
                size: size,
                offset: new THREE.Vector2(offsetX + size, offsetY + size),
                color: '0x'+Math.floor(Math.random()*16777215).toString(16),
                scale: this.props.scale
            };

            var propsNode4 = {
                parent: this,
                min: new THREE.Vector2(xMid, this.props.min.y),
                max: new THREE.Vector2(this.props.max.x, yMid),
                depth: depth,
                size: size,
                offset: new THREE.Vector2(offsetX + size, offsetY),
                color: '0x'+Math.floor(Math.random()*16777215).toString(16),
                scale: this.props.scale
            };

            this.children = [
                new ChunkNode(this.sceneObject, nodeList, propsNode1, this.gameEngine, this.heightmap),
                new ChunkNode(this.sceneObject, nodeList, propsNode2, this.gameEngine, this.heightmap),
                new ChunkNode(this.sceneObject, nodeList, propsNode3, this.gameEngine, this.heightmap),
                new ChunkNode(this.sceneObject, nodeList, propsNode4, this.gameEngine, this.heightmap)
            ];
        }

        nodeList.push(this);
        return this;
    };

    /**
     * Returns the screen error.
     *
     * @method getScreenError
     * @returns {float}
     */
    proto.getScreenError = function() {
        var b = this.graphicsDriver.getCurrentCamera().getCameraContext().position;
        var d = this.props.geometry.boundingSphere.distanceToPoint(b);

        if (d < 0) {
            d = 0;
        }

        return this.lod/d;
    };

    /**
     * Updates a chunk of terrain.
     *
     * @method updates
     * @returns {void}
     */
    proto.update = function() {
        var settings = this.graphicsDriver.getSettings();
        var parent = this.props.parent;
        var inFrustum = false;

        if (!settings.terrainEnableLOD) {
            return;
        }

        var chunk = this.chunk;
        var frustum = this.graphicsDriver.getCurrentCamera()
                                         .getFrustum()
                                         .update()
                                         .getFrustumContext();

        inFrustum = frustum.intersectsSphere(this.props.geometry.boundingSphere);

        if (chunk && inFrustum) {
            var chunkScreenError = this.getScreenError();

            if (chunkScreenError <= 32 || this.children.length === 0) {
                this.chunk.material.visible = true;

                if (!settings.chunkNodeLoading) {
                    if (!this.isNormalsComputed) {
                        this.gameEngine.assetLoader.addAssetCallback(
                            settings.TERRAIN_COMPUTE_NORMALS,
                            this.chunk.uuid,
                            this.computeNormals.bind(this)
                        );
                    }
                } else if (parent != null) {
                    parent.chunk.material.visible = true;
                    settings.totalVerticesRendered += parent.props.geometry.vertices.length;
                    return;
                } else {
                    parent.chunk.material.visible = false;
                }

                settings.totalVerticesRendered += this.props.geometry.vertices.length;

                // Hide children material.
                if (this.children.length !== 0) {
                    this.children[0].hide();
                    this.children[1].hide();
                    this.children[2].hide();
                    this.children[3].hide();
                }
            } else {
                this.chunk.material.visible = false;

                if (this.children.length !== 0) {
                    this.children[0].update();
                    this.children[1].update();
                    this.children[2].update();
                    this.children[3].update();
                }
            }
        } else {
            if (!settings.chunkNodeLoading) {
                this.chunk.material.visible = false;
            }
        }

        return this;
    };

    /**
     * Prevents the children nodes from rendering.
     *
     * @method hide
     * @chainable
     */
    proto.hide = function() {
        this.chunk.material.visible = false;

        if (this.children.length !== 0) {
            this.children[0].hide();
            this.children[1].hide();
            this.children[2].hide();
            this.children[3].hide();
        }

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Helpers
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Loads and builds the chunk data.
     *
     * @method loadChunkData
     * @chainable
     */
    proto.loadChunkData = function() {
        this.isChunkDataLoading = true;

        if (this.chunkDataLoaded) {
            return;
        }

        this.isChunkDataLoading = true;
        this.graphicsDriver.getSettings().chunkNodeLoading = true;

        var depth = this.props.depth;
        var resolution = this.props.size;
        var step = Math.pow(2, depth);
        var i = 0;
        var j = 0;

        this.props.geometry = new THREE.Geometry();

        var geoSize = resolution;
        var tileSize = step;
        var cnt = -1;
        var heightmapData = this.heightmap.getHeightmapData();

        for (i = 0; i < geoSize; i+=step) {
            for (j = 0; j < geoSize; j+=step) {
                var beginX = ((i/step) * tileSize) + this.props.offset.x;
                var beginY = ((j/step) * tileSize) + this.props.offset.y;

                var heightmapIndex1 = (beginX + tileSize) * this.heightmap.getHeight() + (beginY + tileSize);
                var heightmapIndex2 = (beginX) * this.heightmap.getHeight() + (beginY + tileSize);
                var heightmapIndex3 = (beginX) * this.heightmap.getHeight() + (beginY);
                var heightmapIndex4 = (beginX + tileSize) * this.heightmap.getHeight() + (beginY);

                var x1 = (beginX + tileSize) * this.props.scale;
                var y1 = (beginY + tileSize) * this.props.scale;

                var x2 = beginX * this.props.scale;
                var y2 = (beginY + tileSize) * this.props.scale;

                var x3 = beginX * this.props.scale;
                var y3 = beginY * this.props.scale;

                var x4 = (beginX + tileSize) * this.props.scale;
                var y4 = beginY * this.props.scale;

                this.props.geometry.vertices.push(
                    new THREE.Vector3(x1, y1, heightmapData[heightmapIndex1]),
                    new THREE.Vector3(x2, y2, heightmapData[heightmapIndex2]),
                    new THREE.Vector3(x3, y3, heightmapData[heightmapIndex3]),

                    new THREE.Vector3(x3, y3, heightmapData[heightmapIndex3]),
                    new THREE.Vector3(x4, y4, heightmapData[heightmapIndex4]),
                    new THREE.Vector3(x1, y1, heightmapData[heightmapIndex1])
                );

                this.props.geometry.faces.push(
                    new THREE.Face3(++cnt, ++cnt, ++cnt),
                    new THREE.Face3(++cnt, ++cnt, ++cnt)
                );
            }
        }

        this.props.geometry.computeBoundingSphere();

        this.chunk = new THREE.Mesh(this.props.geometry, new THREE.ShaderMaterial({
            doubleSided: true,
            visible: false,
            vertexShader: terrainVertShader.value,
            fragmentShader: terrainFragShader.value
        }));

        this.sceneObject.add(this.chunk);

        this.isChunkDataLoaded = true;
        this.isChunkDataLoading = false;
        this.graphicsDriver.getSettings().chunkNodeLoading = false;
    };

    /**
     * Computes the normals for the chunk geometry.
     *
     * @method computeNormals
     * @chainable
     */
    proto.computeNormals = function() {
        if (this.isNormalsComputed) {
            return;
        }

        var settings = this.gameEngine.getSettings();

        this.props.geometry.computeFaceNormals();
        this.props.geometry.computeVertexNormals();
        this.props.geometry.computeMorphNormals();
        this.props.geometry.normalsNeedUpdate = true;

        this.isNormalsComputed = true;
        settings.chunkNodeLoading = false;

        return this;
    };

    return ChunkNode;
});

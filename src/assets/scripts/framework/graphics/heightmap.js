define(function(require) {
    'use strict';

    var $ = require('jquery');

    /**
     * This class handles loading and returning heightmap
     * data.
     *
     * @class Heightmap
     * @constructor
     */
    var Heightmap = function(gameEngine) {
        /**
         * The game engine.
         *
         * @property gameEngine
         * @type {App}
         */
        this.gameEngine = gameEngine;

        /**
          * The graphics driver.
          *
          * @property graphicsDriver
          * @type {GraphicsDriver}
          */
        this.graphicsDriver = gameEngine.getGraphicsDriver();

        /**
         * Reference to the loaded image.
         *
         * @property image
         * @type {Image}
         */
        this.image = null;

        /**
         * The deferred for when the image load event.
         *
         * @property deferred
         * @type {jQuery.Deferred}
         */
        this.deferred = null;

        /**
         * The heightmap data.
         *
         * @property heightmapData
         * @type {Array}
         */
        this.heightmapData = null;

        /**
         * The width of the heightmap.
         *
         * @property width
         * @type {int}
         */
        this.width = 0;

        /**
         * The height of the heightmap.
         *
         * @property height
         * @type {int}
         */
        this.height = 0;

        /**
         * Flag that checks whether or not this component
         * is currently enabled.
         *
         * @property isEnabled
         * @type Boolean
         */
        this.isEnabled = false;
    };
    var proto = Heightmap.prototype;

    /**
     * Initialize the heightmap.
     *
     * @method init
     * @returns {jQuery.$promise}
     */
    proto.init = function() {
        var settings = this.gameEngine.getSettings();
        this.deferred = new $.Deferred();

        this.setupChildren()
            .setupEventHandlers()
            .enable();

        this.image.src = settings.terrainHeightmap;

        return this.deferred.promise();
    };

    /**
     * Creates/assigns the children objects
     * of the component.
     *
     * @method setupChildren
     * @chainable
     * @private
     */
    proto.setupChildren = function() {
        this.$window = $(window);
        this.image = new Image();

        return this;
    };

    /**
     * Links up the handlers to their events.
     *
     * @method setupEventHandlers
     * @chainable
     */
    proto.setupEventHandlers = function() {
        this.imageLoadHandler = this.onLoad.bind(this);
        this.image.onload = this.imageLoadHandler;

        return this;
    };

    /**
     * Enables the component and all of the events.
     *
     * @method enable
     * @chainable
     */
    proto.enable = function() {
        if (this.isEnabled) {
            return this;
        }

        this.$window.on('heightmapLoad', this.imageLoadHandler);

        this.isEnabled = true;
        return this;
    };

    /**
     * Disables the component and all events associated
     * with the component.
     *
     * @method disable
     * @chainable
     */
    proto.disable = function() {
        if (!this.isEnabled) {
            return this;
        }

        this.$window.off('heightmapLoad', this.imageLoadHandler);

        this.isEnabled = false;
        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Events
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Handles the event for when the image has
     * finished loading.
     *
     * @method onLoad
     */
    proto.onLoad = function() {
        this.loadHeightmapData();

        this.$window.trigger('heighmapLoaded');
        this.deferred.done();
    };

    ///////////////////////////////////////////////////////////////////////////
    // Helpers
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Loads the heightmap data from the image.
     *
     * @method loadHeightmapData
     * @returns {Array}
     */
    proto.loadHeightmapData = function() {
        var scale = this.gameEngine.getSettings().heightmapHeightScale;
        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');
        var imageData = null;
        var pixelData = null;
        var size = 0;
        var i = 0;
        var j = 0;

        canvas.width = this.image.width;
        canvas.height = this.image.height;

        this.width = canvas.width;
        this.height = canvas.height;

        size = canvas.width * canvas.height;
        this.heightmapData = new Float32Array(size);

        context.drawImage(this.image, 0, 0);

        for (; i < size; i++) {
            this.heightmapData[i] = 0;
        }

        imageData = context.getImageData(0, 0, this.image.width, this.image.height);
        pixelData = imageData.data;

        for (i = 0; i < pixelData.length; i += 4) {
            var all = pixelData[i] + pixelData[i+1] + pixelData[i+2];
            this.heightmapData[j++] = all/(scale);
        }

        canvas.remove();
        canvas = null;

        return this.heightmapData;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the heightmap data.
     *
     * @method getHeightmapData
     * @returns {Array}
     */
    proto.getHeightmapData = function() {
        return this.heightmapData;
    };

    /**
     * Returns the width of the heightmap.
     *
     * @method getWidth
     * @returns {int}
     */
    proto.getWidth = function() {
        return this.width;
    };

    /**
     * Returns the height of the heightmap.
     *
     * @method getHeight
     * @returns {int}
     */
    proto.getHeight = function() {
        return this.height;
    };

    return Heightmap;
});

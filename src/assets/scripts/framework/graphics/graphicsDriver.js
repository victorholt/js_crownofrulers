define(function(require) {
    'use strict';

    var Viewport = require('./viewport');
    var THREE = require('threejs');
    var _ = require('underscore');
    var $ = require('jquery');

    /**
     * The graphics driver class for handling rendering using Canvas
     * or WebGL.
     *
     * @class GraphicsDriver
     * @param {jQuery<HTMLElement>} $element
     * @constructor
     */
    var GraphicsDriver = function($element, gameEngine) {
        /**
         * The DOM reference for rendering.
         *
         * @property $element
         * @type {jQuery<HTMLElement>}
         */
        this.$element = $element;

        /**
         * The game engine.
         *
         * @property gameEngine
         * @type {App}
         */
        this.gameEngine = gameEngine;

        /**
         * Tracks whether or not our graphics driver is enabled.
         *
         * @property isEnabled
         * @type {Boolean}
         */
        this.isEnabled = false;

        /**
         * The reference to the WebGL or Canvas rendering context.
         *
         * @property renderContext
         * @type {THREE.Renderer<WebGL|Canvas>}
         */
        this.renderContext = null;

        /**
         * The driver type used to determine how to render the graphics.
         *
         * @property driverType
         * @type {String}
         */
        this.driverType = '';

        /**
         * The viewport class containing information about
         * the user's window and device.
         *
         * @property viewport
         * @type {Viewport}
         */
        this.viewport = null;

        /**
         * The current Scene object.
         *
         * @property currentScene
         * @type {Scene}
         */
        this.currentScene = null;

        /**
         * The current camera object.
         *
         * @property currentCamera
         * @type {Camera}
         */
        this.currentCamera = null;

        /**
         * The game engine settings.
         *
         * @property settings
         * @type {Settings}
         */
        this.settings = gameEngine.getSettings();
    };

    var proto = GraphicsDriver.prototype;

    /**
     * Graphics driver definition for rendering with canvas.
     *
     * @property GD_CANVAS_TYPE
     * @type {String}
     * @static
     */
    GraphicsDriver.GD_CANVAS_TYPE = 'canvas';

    /**
      * Graphics driver definition for rendering with webgl.
      *
      * @property GD_WEBGL_TYPE
      * @type {String}
      * @static
      */
    GraphicsDriver.GD_WEBGL_TYPE = 'webgl';

    /**
     * Initializes the graphics driver class based upon
     * the given type.
     *
     * @method init
     * @chainable
     */
    proto.init = function(driverType) {
        this.viewport = new Viewport(window.innerWidth, window.innerHeight);
        this.driverType = driverType;

        switch(driverType) {
            case GraphicsDriver.GD_CANVAS_TYPE:
                break;
            case GraphicsDriver.GD_WEBGL_TYPE:
                this.renderContext = new THREE.WebGLRenderer();
                this.renderContext.setSize(this.viewport.getWidth(),
                                           this.viewport.getHeight());
                break;
        }

        this.$element.append(this.renderContext.domElement);
        this.setupEventHandlers()
            .setupChildren()
            .enable();

        return this;
    };

    /**
     * Links up the handlers to their events.
     *
     * @method setupEventHandlers
     * @chainable
     * @private
     */
    proto.setupEventHandlers = function() {
        this.resizeEventHandler = this.onResizeEvent.bind(this);

        return this;
    };

    /**
     * Creates/assigns the children objects
     * of the component.
     *
     * @method setupChildren
     * @chainable
     * @private
     */
    proto.setupChildren = function() {
        this.$window = $(window);

        return this;
    };

    /**
     * Enables the class and events.
     *
     * @method enable
     * @chainable
     */
    proto.enable = function() {
        if (this.isEnabled) {
            return this;
        }

        // Enable the events.
        this.$window.on('resize', this.resizeEventHandler);

        this.isEnabled = true;

        return this;
    };

    /**
     * Disables the class and events.
     *
     * @method disable
     * @chainable
     */
    proto.disable = function() {
        if (!this.isEnabled) {
            return this;
        }

        // Disable the events.
        this.$window.off('resize', this.resizeEventHandler);

        this.isEnabled = false;

        return this;
    };

    /**
     * Renders the given scene.
     *
     * @method render
     * @param {Scene} scene
     * @param {Camera} camera
     * @chainable
     */
    proto.render = function() {
        if (!this.isEnabled) {
            return this;
        }

        if (!this.currentCamera || !this.currentScene) {
            return this;
        }

        var camera = this.currentCamera;
        var scene = this.currentScene;

        this.getRenderContext().render(
            scene.getSceneContext(),
            camera.getCameraContext()
        );
        requestAnimationFrame(this.render.bind(this));

        // Update the current scene.
        if (scene.update && _.isFunction(scene.update)) {
            scene.update();
        }

        // Update the UI elements.
        this.gameEngine.getUIManager().update();

        // Reset the settings stats.
        this.getSettings().totalVerticesRendered = 0;

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // EVENTS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Handles the resize event.
     *
     * @method onResizeEvent
     */
    proto.onResizeEvent = function() {
        if (!this.currentCamera) {
            return;
        }

        var camera = this.currentCamera;

        camera.getCameraContext().aspect = this.getViewport().getWidth() /
                                    this.getViewport().getHeight();
        camera.getCameraContext().updateProjectionMatrix();

        this.getRenderContext().setSize(this.getViewport().getWidth(),
                                        this.getViewport().getHeight());

        this.render();
    };

    ///////////////////////////////////////////////////////////////////////////
    // HELPERS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Setups up the current camera and scene objects.
     *
     * @method setupCameraScene
     * @chainable
     */
    proto.setupCameraScene = function(camera, scene) {
        this.currentCamera = camera;
        this.currentScene = scene;

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Return the game engine settings.
     *
     * @method getSettings
     * @returns {Settings}
     */
    proto.getSettings = function() {
        return this.settings;
    };

    /**
     * Returns the ThreeJS renderer.
     *
     * @method getRenderer
     * @returns {object}
     */
    proto.getRenderContext = function() {
        return this.renderContext;
    };

    /**
     * Returns the Viewport object.
     *
     * @method getViewport
     * @returns {Viewport}
     */
    proto.getViewport = function() {
        return this.viewport;
    };

    /**
     * Returns the current active scene.
     *
     * @method getCurrentScene
     * @returns {Scene}
     */
    proto.getCurrentScene = function() {
        return this.currentScene;
    };

    /**
     * Sets the current scene.
     *
     * @method setCurrentScene
     * @param Scene scene
     * @chainable
     */
    proto.setCurrentScene = function(scene) {
        this.currentScene = scene;

        return this;
    };

    /**
     * Returns the current active camera.
     *
     * @method getCurrentCamera
     * @returns {Camera}
     */
    proto.getCurrentCamera = function() {
        return this.currentCamera;
    };

    /**
     * Sets the current camera.
     *
     * @method setCurrentCamera
     * @param Camera camera
     * @chainable
     */
    proto.setCurrentCamera = function(camera) {
        this.currentCamera = camera;

        return this;
    };

    return GraphicsDriver;
});

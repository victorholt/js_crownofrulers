define(function(require) {
    'use strict';

    var $ = require('jquery');

    /**
     * Stores viewport information of the user's current
     * device.
     *
     * @class Viewport
     * @param {int} width
     * @param {int} height
     * @constructor
     */
    var Viewport = function(width, height) {
        /**
         * Tracks whether or not the viewport is enabled.
         *
         * @property isEnabled
         * @type {Boolean}
         */
        this.isEnabled = false;

        /**
         * The current width of the viewport.
         *
         * @property width
         * @type {int}
         */
        this.width = width;

        /**
         * The current height of the viewport.
         *
         * @property height
         * @type {int}
         */
        this.height = height;

        /**
         * The width of the current device.
         *
         * @property deviceWidth
         * @type {int}
         */
        this.deviceWidth = 0;

        /**
         * The height of the current device.
         *
         * @property deviceHeight
         * @type {int}
         */
        this.deviceHeight = 0;

        /**
         * The window width of the browser.
         *
         * @property windowWidth
         * @type {int}
         */
        this.windowWidth = 0;

        /**
         * The window height of the browser.
         *
         * @property windowHeight
         * @type {int}
         */
        this.windowHeight = 0;
    };

    var proto = Viewport.prototype;

    /**
     * Initializes the object properties.
     *
     * @method init
     * @chainable
     */
    proto.init = function(width, height) {
        this.width = width;
        this.height = height;

        this.setupChildren()
            .setupEventHandlers()
            .enable();

        return this;
    };

    /**
     * Creates/assigns the children objects
     * of the component.
     *
     * @method setupChildren
     * @returns {SearchView}
     * @private
     */
    proto.setupChildren = function() {
        this.$window = $(window);

        return this;
    };

    /**
     * Removes the children objects of the component.
     *
     * @method removeChildren
     * @returns {SearchView}
     * @private
     */
    proto.removeChildren = function() {
        return this;
    };

    /**
     * Links up the handlers to their events.
     *
     * @method setupEventHandlers
     * @returns {SearchView}
     * @private
     */
    proto.setupEventHandlers = function() {
        this.windowResizeHandler = this.onWindowResize.bind(this);

        return this;
    };

    /**
     * Enables the object and associated events.
     *
     * @method enable
     * @chainable
     */
    proto.enable = function() {
        if (this.isEnabled) {
            return this;
        }

        this.$window.on('resize', this.windowResizeHandler);

        return this;
    };

    /**
     * Disables the object and associated events.
     *
     * @method disable
     * @chainable
     */
    proto.disable = function() {
        if (!this.isEnabled) {
            return this;
        }

        this.$window.off('resize', this.windowResizeHandler);

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Events
    ///////////////////////////////////////////////////////////////////////////

    /**
     * This event triggers upon a window resize event and
     * updates the viewport properties.
     *
     * @method onWindowResize
     * @returns {void}
     */
    proto.onWindowResize = function() {
        this.deviceWidth = window.screen.width;
        this.deviceHeight = window.screen.height;
        this.windowWidth = window.width;
        this.windowHeight = window.height;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the width of the viewport.
     *
     * @method getWidth
     * @returns {int}
     */
    proto.getWidth = function() {
        return this.width;
    };

    /**
     * Sets the width of the viewport.
     *
     * @method setWidth
     * @param {int} width
     * @chainable
     */
    proto.setWidth = function(width) {
        this.width = width;

        return this;
    };

    /**
     * Returns the height of the viewport.
     *
     * @method getHeight
     * @returns {int}
     */
    proto.getHeight = function() {
        return this.height;
    };

    /**
     * Sets the height of the viewport.
     *
     * @method setHeight
     * @param {int} height
     * @chainable
     */
    proto.setHeight = function(height) {
        this.height = height;

        return this;
    };

    /**
     * Returns the device width.
     *
     * @method getDeviceWidth
     * @returns {int}
     */
    proto.getDeviceWidth = function() {
        return this.deviceWidth;
    };

    /**
     * Returns the device height.
     *
     * @method getDeviceHeight
     * @returns {int}
     */
    proto.getDeviceHeight = function() {
        return this.deviceHeight;
    };

    /**
     * Returns the window width.
     *
     * @method getWindowWidth
     * @returns {int}
     */
    proto.getWindowWidth = function() {
        return this.windowWidth;
    };

    /**
     * Returns the window height.
     *
     * @method getWindowHeight
     * @returns {int}
     */
    proto.getWindowHeight = function() {
        return this.windowHeight;
    };

    return Viewport;
});

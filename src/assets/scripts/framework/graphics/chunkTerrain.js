define(function(require) {
    'use strict';

    var THREE = require('threejs');
    var ChunkNode = require('./chunkNode');

    /**
     * The terrain engine using chunks to render the mesh.
     *
     * @class ChunkTerrain
     * @constructor
     */
    var ChunkTerrain = function(gameEngine, heightmap) {
        /**
         * The game engine.
         *
         * @property gameEngine
         * @type {App}
         */
        this.gameEngine = gameEngine;

        /**
         * The graphics driver.
         *
         * @property graphicsDriver
         * @type {GraphicsDriver}
         */
        this.graphicsDriver = gameEngine.getGraphicsDriver();

        /**
         * The heightmap for the terrain.
         *
         * @property heightmap
         * @type {Heightmap}
         */
        this.heightmap = heightmap;

        /**
         * The node list of all the leafs in the quadtree.
         *
         * @property treeNodeList
         * @type {Array<ChunkNode>}
         */
        this.treeNodeList = [];

        /**
         * The primary node.
         *
         * @property chunkNode
         * @type {ChunkNode}
         */
        this.chunkNode = null;

        /**
         * The starting offset position of the terrain.
         *
         * @property offset
         * @type {THREE.Vector3}
         */
        this.offset = new THREE.Vector3(0, 0, 0);

        /**
         * The scene object.
         *
         * @property sceneObject
         * @type {THREE.Object3D}
         */
        this.sceneObject = new THREE.Object3D();
    };
    var proto = ChunkTerrain.prototype;

    /**
     * Initializes the terrain engine.
     *
     * @method init
     * @param {int} resolution
     * @param {int} depth
     * @chainable
     */
    proto.init = function(resolution, depth, scale) {
        this.updateViewport();

        var props = {
            geometry: null,
            min: new THREE.Vector2(0, 0),
            max: new THREE.Vector2(resolution, resolution),
            depth: depth,
            size: resolution,
            offset: new THREE.Vector2(0, 0),
            scale: scale
        };

        this.chunkNode = new ChunkNode(this.sceneObject,
                                       this.treeNodeList,
                                       props,
                                       this.gameEngine,
                                       this.heightmap);

        return this;
    };

    /**
     * Updates the terrain chunks.
     *
     * @method update
     * @chainable
     */
    proto.update = function() {
        this.chunkNode.update();

        return this;
    };

    /**
     * Updates the terrain viewport setting value for LOD.
     *
     * @method updateViewport
     * @chainable
     */
    proto.updateViewport = function() {
        // @see http://stackoverflow.com/questions/13350875/three-js-width-of-view
        // @see http://stackoverflow.com/questions/17837652/calculating-frame-and-aspect-ratio-guides-to-match-cameras

        var camera = this.graphicsDriver.getCurrentCamera().getCameraContext();
        camera.updateProjectionMatrix();

        var scale = 8;

        var viewportWidth = this.graphicsDriver.getViewport().getWidth() / scale;
        var viewportHeight = this.graphicsDriver.getViewport().getHeight() / scale;
        var aspectRatio = viewportWidth / viewportHeight;

        var fov = camera.fov;
        var vFOV = fov * Math.PI / 180;
        var hFOV = 2 * Math.atan(Math.tan(vFOV / 2) * aspectRatio);

        this.gameEngine.getSettings().terrainViewport = viewportWidth / (2 * Math.tan(hFOV/2));
        //this.gameEngine.getSettings().terrainViewport = viewportWidth / (2 * Math.tan((45 * Math.PI/180) / 2));

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the scene object.
     *
     * @method getSceneObject
     * @returns {Object3D}
     */
    proto.getSceneObject = function() {
        return this.sceneObject;
    };

    return ChunkTerrain;
});

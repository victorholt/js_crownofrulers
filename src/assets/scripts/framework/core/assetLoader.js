define(function() {
    'use strict';

    /**
     * Handles loading assets with an interval to prevent
     * screen freezing.
     *
     * @class AssetLoader
     * @constructor
     */
    var AssetLoader = function(gameEngine) {
        /**
         * The game engine.
         *
         * @property gameEngine
         * @type {App}
         */
        this.gameEngine = gameEngine;

        /**
         * The asset queue to load.
         *
         * @property queue
         * @type {Array}
         */
        this.queue = [];

        /**
         * The interval object.
         *
         * @property assetInterval
         * @type {Object<Interval>}
         */
        this.assetInterval = null;

        /**
         * Tracks whether or not the asset queue is watching.
         *
         * @property isWatching
         * @type {boolean}
         */
        this.isWatching = false;

        /**
         * The last time the asset loader was updated.
         *
         * @property lastUpdatedTime
         * @type {int}
         */
        this.lastUpdatedTime = 0;

        this.init();
    };
    var proto = AssetLoader.prototype;

    /**
     * Initializes the interval for loading.
     *
     * @method init
     * @chainable
     */
    proto.init = function() {
        this.startAssetInterval();

        return this;
    };

    /**
     * Adds an asset callback method to the queue.
     *
     * @method addAssetCallback
     * @param {String} groupName
     * @param {String} assetId
     * @param {Object<Function>} loadCallback
     * @param {int} waitToNextLoad
     * @chainable
     */
    proto.addAssetCallback = function(groupName, assetId, loadCallback, waitToNextLoad) {
        var settings = this.gameEngine.getSettings();
        waitToNextLoad = waitToNextLoad != null ? waitToNextLoad : settings.assetLoadingIntervalTime;

        if ((this.queue.length + 1) >= settings.assetMaxQueue) {
            return;
        }

        // Check if this assetId already exists in our array.
        var callback = this.queue.filter(function(assetItem) {
            return (assetItem.id === assetId);
        });

        if (callback.length !== 0) {
            return;
        }

        var asset = {
            'groupName': groupName,
            'id': assetId,
            'type': 'callback',
            'wait': waitToNextLoad,
            'loadCallback': loadCallback
        };

        this.queue.push(asset);

        return this;
    };

    /**
     * Starts the watch for the asset queue.
     *
     * @method startAssetInterval
     * @chainable
     */
    proto.startAssetInterval = function() {
        if (this.isWatching) {
            return;
        }

        var settings = this.gameEngine.getSettings();
        this.assetInterval = setInterval(
            this.watchAssetQueue.bind(this),
            settings.assetWatchIntervalTime
        );

        this.isWatching = true;

        return this;
    };

    /**
     * Watches the queue for updates.
     *
     * @method watchAssetQueue
     * @chainable
     */
    proto.watchAssetQueue = function() {
        if (this.queue.length === 0) {
            return;
        }

        var asset = this.queue.shift();

        switch (asset.type) {
            case 'callback':
                asset.loadCallback();
                break;
        }

        asset = null;
        return this;
    };

    return AssetLoader;
});

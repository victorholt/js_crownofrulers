define(function(require) {
    'use strict';

    var THREE = require('threejs');
    var _ = require('underscore');

    /**
     * The base scene class for the game engine.
     *
     * @class Scene
     * @constructor
     */
    var Scene = function() {
        /**
         * Reference the the THREE.Scene object.
         *
         * @property sceneContext
         * @type {THREE.Scene}
         */
        this.sceneContext = null;

        /**
         * The game engine.
         *
         * @property gameEngine
         * @type {App}
         */
        this.gameEngine = null;

        /**
         * Reference to the GraphicsDriver object.
         *
         * @property graphicsDriver
         * @type {GraphicsDriver}
         */
        this.graphicsDriver = null;

        /**
         * Reference to the currently used camera object.
         *
         * @property camera
         * @type {Camera}
         */
        this.camera = null;

        /**
         * The initialization handle for scenes
         * to tie into the init method.
         *
         * @property initHandle
         * @type {object<Function>}
         * @throws Exception
         */
        this.initHandle = null;

        /**
         * The update handle for scenes
         * to tie into the update method.
         *
         * @property updateHandle
         * @type {object<Function>}
         * @throws Exception
         */
        this.updateHandle = null;

        /**
         * Tracks whether or not the scene is finished
         * loading.
         *
         * @property isLoaded
         * @type {boolean}
         */
        this.isLoaded = false;
    };

    var proto = Scene.prototype;

    /**
     * Initializes the scene.
     *
     * @method init
     * @param {Camera} camera
     * @chainable
     */
    proto.init = function(gameEngine, camera) {
        this.gameEngine = gameEngine;
        this.graphicsDriver = gameEngine.getGraphicsDriver();
        this.camera = camera;
        this.sceneContext = new THREE.Scene();
        this.sceneContext.add(this.camera.getCameraContext());

        if (this.initHandle != null && _.isFunction(this.initHandle)) {
            try {
                this.initHandle();
            } catch(err) {
                throw err;
            }
        }

        return this;
    };

    /**
     * Update the scene.
     *
     * @method update
     * @chainable
     */
    proto.update = function() {
        if (this.updateHandle != null && _.isFunction(this.updateHandle)) {
            try {
                this.updateHandle();
            } catch(err) {
                throw err;
            }
        }

        return this;
    };


    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the THREE scene context.
     *
     * @method getSceneContext
     * @returns {THREE.Scene}
     */
    proto.getSceneContext = function() {
        return this.sceneContext;
    };

    /**
     * Returns the game engine.
     *
     * @method getGameEngine
     * @returns {App}
     */
    proto.getGameEngine = function() {
        return this.gameEngine;
    };

    /**
     * Returns the GraphicsDriver object.
     *
     * @method getGraphicsDriver
     * @returns {GraphicsDriver}
     */
    proto.getGraphicsDriver = function() {
        return this.graphicsDriver;
    };

    /**
     * Returns the camera object.
     *
     * @method getCamera
     * @returns {Camera}
     */
    proto.getCamera = function() {
        return this.camera;
    };

    return Scene;
});

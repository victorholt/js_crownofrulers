define(function() {
    'use strict';

    /**
     * Handles console log messages.
     *
     * @class Logger
     * @constructor
     */
    var Logger = function() {
    };

    /**
     * Logs a message to the console.
     *
     * @method log
     * @param {string} message
     * @chainble
     * @static
     */
    Logger.log = function(message) {
        if (window.console || console) {
            console.log(message);
        }

        return this;
    };

    return Logger;
});

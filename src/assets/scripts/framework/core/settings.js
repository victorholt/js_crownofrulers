define(function() {
    'use strict';

    /**
     * Various game engine settings.
     */
    var Settings = function() {

        ///////////////////////////////////////////////////////////////////////////
        // General Settings
        ///////////////////////////////////////////////////////////////////////////

        /**
         * Flag for displaying debug information.
         *
         * @property showDebugInfo
         * @type {boolean}
         */
        this.showDebugInfo = false;

        ///////////////////////////////////////////////////////////////////////////
        // Graphics Stats
        ///////////////////////////////////////////////////////////////////////////

        /**
         * The total number of vertices that have been process
         * but not necessarily being rendered.
         *
         * @property totalVerticesProcessed
         * @type {int}
         */
        this.totalVerticesProcessed = 0;

        /**
         * The total number of vertices currently being rendered.
         *
         * @property totalVerticesRendered
         * @type {int}
         */
        this.totalVerticesRendered = 0;

        /**
         * Max lod level for a terrain before it is
         * forced to render.
         *
         * @property maxTerrainLODLevel
         * @type {int}
         */
        this.maxTerrainLODLevel = 2;

        ///////////////////////////////////////////////////////////////////////////
        // Assets Settings
        ///////////////////////////////////////////////////////////////////////////

        /**
         * Different loading groups for assets.
         *
         * @property assetGroups
         * @type {Object<String>}
         */
        this.assetGroups = {
            TERRAIN_CHUNK_LOADING: 'Building Terrain Chunks',
            TERRAIN_COMPUTE_NORMALS: 'Computing Vertex Normals'
        };

        /**
         * The asset loading interval between each asset
         * being loaded.
         *
         * @property assetLoadingInterval
         * @type {int}
         */
        this.assetLoadingIntervalTime = 100;

        /**
         * The max that can be held in the asset queue.
         *
         * @property assetMaxQueue
         * @type {int}
         */
        this.assetMaxQueue = 25;

        /**
         * The asset watch interval between each asset
         * being loaded.
         *
         * @property assetWatchIntervalTime
         * @type {int}
         */
        this.assetWatchIntervalTime = 100;

        ///////////////////////////////////////////////////////////////////////////
        // Terrain Settings
        ///////////////////////////////////////////////////////////////////////////

        /**
         * Scaling for the heightmap (1-12 to make mountains).
         *
         * @property heightmapHeightScale
         * @type {int}
         */
        this.heightmapHeightScale = 4;

        /**
         * Scaling for the heightmap (larger stretches the map).
         *
         * @property heightmapWidthScale
         * @type {int}
         */
        this.heightmapWidthScale = 5;

        /**
         * The terrain viewport setting for LOD calculating.
         *
         * @property terrainViewport
         * @type {float}
         */
        this.terrainViewport = 0;

        /**
         * The heightmap image for the terrain.
         *
         * @property terrainHeightmap
         * @type {string}
         */
        this.terrainHeightmap = 'assets/media/common/images/maps/demo/demo_heightmap2.png';
        //this.terrainHeightmap = 'assets/media/common/images/maps/demo/demo_europe1.png';

        /**
         * Tracks whether or not we can perform LOD.
         *
         * @property terrainEnableLOD
         * @type {boolean}
         */
        this.terrainEnableLOD = true;

        /**
         * Tracks whether or not we can perform quadtree culling.
         *
         * @property terrainEnableCulling
         * @type {boolean}
         */
        this.terrainEnableCulling = true;

        /**
         * Tracks whether or not a chunk is currently loading.
         *
         * @property chunkNodeLoading
         * @type {boolean}
         */
        this.chunkNodeLoading = false;

    };
    //var proto = Settings.prototype;

    return Settings;
});

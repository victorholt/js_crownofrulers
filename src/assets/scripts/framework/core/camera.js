define(function(require) {
    'use strict';

    var THREE = require('threejs');
    var Frustum = require('./../utils/frustum');

    /**
     * The base camera class for the game engine.
     *
     * @class Camera
     * @param {Viewport} viewport
     * @constructor
     */
    var Camera = function(viewport) {
        /**
         * Reference to the Viewport graphics driver object.
         *
         * @property viewport
         * @type {Viewport}
         */
        this.viewport = viewport;

        /**
         * Reference the the THREE.Camera object.
         *
         * @property cameraContext
         * @type {THREE.Camera}
         */
        this.cameraContext = null;

        /**
         * The camera type that is created.
         *
         * @property cameraType
         * @type {String}
         */
        this.cameraType = '';

        /**
         * The view angle for the camera.
         *
         * @property viewAngle
         * @type {float}
         */
        this.viewAngle = 45;

        /**
         * The camera aspect ratio.
         *
         * @property aspectRatio
         * @type {float}
         */
        this.aspectRatio = 0;

        /**
         * The camera perspective near view.
         *
         * @property nearView
         * @type {float}
         */
        this.nearView = 0.1;

        /**
         * The camera prespective far view limit.
         *
         * @property farView
         * @type {float}
         */
        this.farView = 10000;

        /**
         * The Frustum object for culling and rendering.
         *
         * @property frustum
         * @type {Frustum}
         */
        this.frustum = null;

        /**
         * Flag that checks whether or not this component
         * is currently enabled.
         *
         * @property isEnabled
         * @type Boolean
         */
        this.isEnabled = false;
    };

    var proto = Camera.prototype;

    /**
     * Prespective definition for the camera.
     *
     * @property PRESPECTIVE
     * @type {String}
     * @static
     */
    Camera.PERSPECTIVE = 'prespective';

    /**
     * Initializes the camera.
     *
     * @method init
     * @param {String} cameraType
     * @chainable
     */
    proto.init = function(cameraType, aspectRatio, viewAngle, nearView, farView) {
        // Update the aspectRatio based on the width/height of the
        // main DOM view.
        this.aspectRatio = this.viewport.getWidth() / this.viewport.getHeight();

        // Set the properties.
        this.cameraType = cameraType != null ? cameraType : Camera.PERSPECTIVE;
        this.viewAngle = viewAngle != null ? viewAngle : this.viewAngle;
        this.aspectRatio = aspectRatio != null ? aspectRatio : this.aspectRatio;
        this.nearView = nearView != null ? nearView : this.nearView;
        this.farView = farView != null ? farView : this.farView;

        switch(cameraType) {
            case Camera.PRESPECTIVE:
                this.cameraContext = new THREE.PerspectiveCamera(
                    this.viewAngle,
                    this.aspectRatio,
                    this.nearView,
                    this.farView
                );
                break;
        }
        this.cameraContext.updateProjectionMatrix();
        this.frustum = new Frustum(this);

        this.enable();
    };

    /**
     * Enables the component and all of the events.
     *
     * @method enable
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.enable = function() {
        if (this.isEnabled) {
            return this;
        }

        // Enable the camera.
        this.isEnabled = true;

        return this;
    };

    /**
     * Disables the component and all events associated
     * with the component.
     *
     * @method disable
     * @returns {CameraTrackballControls}
     * @private
     */
    proto.disable = function() {
        if (!this.isEnabled) {
            return this;
        }

        this.isEnabled = false;
        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Getters/Setters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the THREE camera context.
     *
     * @method getCameraContext
     * @returns {THREE.Camera}
     */
    proto.getCameraContext = function() {
        return this.cameraContext;
    };

    /**
     * Returns the Frustum object.
     *
     * @method getFrustum
     * @returns {Frustum}
     */
    proto.getFrustum = function() {
        return this.frustum;
    };

    return Camera;
});

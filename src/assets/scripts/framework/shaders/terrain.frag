// QuadTree vertex fragment.
// @author Victor Holt

varying vec3 vNormal;

void main() {
    vec3 light = vec3(1.0, 1.0, 1.0);
    light = normalize(light);

    float dProd = max(0.0, dot(vNormal, light));

    gl_FragColor = vec4(dProd, dProd, dProd, 1.0);
}

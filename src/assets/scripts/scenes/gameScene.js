define(function(require) {
    'use strict';

    var Scene = require('./../framework/core/scene');
    var TrackballControls = require('./../framework/input/_cameraTrackballControls');
    var $ = require('jquery');
    var logger = require('logger');
    //var TrackballControls = require('./../framework/input/cameraTrackballControls');

    /**
     * The main game scene.
     *
     * @class GameScene
     * @constructor
     */
    var GameScene = function() {
    };

    // Inherit the Scene class.
    GameScene.prototype = Object.create(Scene.prototype);
    var proto = GameScene.prototype;

    /**
     * Creates/assigns the children objects
     * of the component.
     *
     * @method setupChildren
     * @chainable
     * @private
     */
    proto.setupChildren = function() {
        this.$window = $(window);

        return this;
    };

    /**
     * Links up the handlers to their events.
     *
     * @method setupEventHandlers
     * @chainable
     */
    proto.setupEventHandlers = function() {
        this.loadTerrainHandler = this.onLoadTerrain.bind(this);

        this.$window.on('heighmapLoaded', this.loadTerrainHandler);

        return this;
    };

    /**
     * Initializes and enables the class.
     *
     * @method initHandle
     * @chainable
     */
    proto.initHandle = function() {
        this.setupChildren()
            .setupEventHandlers();

        // Load the heightmap.
        var HeightMap = require('./../framework/graphics/heightmap');
        this.heightmap = new HeightMap(this.getGameEngine());
        this.heightmap.init();

        this.getCamera().getCameraContext().position.z = 100;

        this.trackballControls = new TrackballControls(this.getCamera().getCameraContext());

        this.trackballControls.keys = [ 65, 83, 68 ];

        this.trackballControls.rotateSpeed = 1.0;
		this.trackballControls.zoomSpeed = 1.2;
		this.trackballControls.panSpeed = 0.8;

		//this.trackballControls.enableZoom = true;
		//this.trackballControls.enablePan = true;

        this.trackballControls.noZoom = false;
        this.trackballControls.noPan = false;

		this.trackballControls.staticMoving = true;
		this.trackballControls.dynamicDampingFactor = 0.3;

        return this;
    };

    /**
     * Updates the scene.
     *
     * @method updateHandle
     * @chainable
     */
    proto.updateHandle = function() {
        if (!this.isLoaded) {
            return this;
        }

        this.trackballControls.update();

        if (this.chunkTerrain != null) {
            this.chunkTerrain.update();
        }

        return this;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Events
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Handles loading and creating the terrain.
     *
     * @method onLoadTerrain
     */
    proto.onLoadTerrain = function() {
        var heightmapWidthScale = this.getGameEngine().getSettings().heightmapWidthScale;
        var ChunkTerrain = require('./../framework/graphics/chunkTerrain');

        this.chunkTerrain = new ChunkTerrain(this.getGameEngine(), this.heightmap);

        // Heightmap 1
        //this.chunkTerrain.init(256, 2, heightmapWidthScale);

        // Heightmap 2
        this.chunkTerrain.init(400, 4, heightmapWidthScale);

        // Europe1
        //this.chunkTerrain.init(900, 3, heightmapWidthScale);

        //this.chunkTerrain.init(128, 2, heightmapWidthScale);
        this.getSceneContext().add(this.chunkTerrain.getSceneObject());

        logger.log('Terrain loaded into scene');
        this.isLoaded = true;
    };

    return GameScene;
});

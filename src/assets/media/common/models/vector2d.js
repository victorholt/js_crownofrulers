define(function(require) {
    'use strict';

    /**
     * Model that represents a 2D vector point.
     *
     * @class Vector2D
     * @param {int|float|double} x
     * @param {int|float|double} y
     * @constructor
     */
    var Vector2D = function(x, y) {
        /**
         * The x position.
         *
         * @property x
         * @type {int|float|double}
         * @public
         */
        this.x = x;

        /**
         * The y position.
         *
         * @property y
         * @type {int|float|double}
         * @public
         */
        this.y = y;
    };

    return Vector2D;
});

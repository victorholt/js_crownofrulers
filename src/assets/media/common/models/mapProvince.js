define(function(require) {
    'use strict';

    var Vector2D = require('./vector2d');

    /**
     * Model that represents a single province and its points.
     *
     * @class MapProvince
     * @constructor
     */
    var MapProvince = function() {
        /**
         * An array of 2D points.
         *
         * @property points
         * @type {Array<Vector2D>}
         */
        this.points = [];
    };

    var proto = MapProvince.prototype;

    /**
     * Adds a point to the province.
     *
     * @method addPoint
     * @param {int|float|double} x
     * @param {int|float|double} y
     * @chainable
     */
    proto.addPoint = function(x, y) {
        this.points.push(new Vector2D(x, y));

        return this;
    };

    return MapProvince;
});
